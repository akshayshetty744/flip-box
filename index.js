let childOne = document.querySelector(".div1");
childOne.style.background = "#C66F33";
childOne.style.width = "42%";
childOne.style.display = "flex";
childOne.style.flexWrap = "wrap";
childOne.style.justifyContent = "center";

for (let index = 0; index < 12; index++) {
  //without using Event DelIgation
  let childDiv = document.createElement("div");
  childDiv.classList.add("superChild1");
  childDiv.style.width = "100px";
  childDiv.style.height = "100px";
  childDiv.style.background = "#444444";
  childDiv.style.margin = "15px";
  childOne.appendChild(childDiv);

  let class1 = document.querySelectorAll(".superChild1");
  for (let index = 0; index < class1.length; index++) {
    class1[index].addEventListener("click", () => {
      class1[index].textContent = index + 1;
      setTimeout(() => {
        class1[index].textContent = "";
      }, 5000);
      class1[index].style.color = "white";
      class1[index].style.fontSize = "40px";
      class1[index].style.textAlign = "center";
      class1[index].style.display = "flex";
      class1[index].style.justifyContent = "center";
      class1[index].style.alignItems = "center";
    });
  }

  // using Event DelIgation
  let childDiv2;
  let childTwo;
  childTwo = document.querySelector(".div2");
  childTwo.style.background = "#C66F33";
  childTwo.style.width = "42%";
  childTwo.style.display = "flex";
  childTwo.style.flexWrap = "wrap";
  childTwo.style.justifyContent = "center";

  childDiv2 = document.createElement("div");
  childDiv2.classList.add(index + 1);
  childDiv2.style.width = "100px";
  childDiv2.style.height = "100px";
  childDiv2.style.background = "#444444";
  childDiv2.style.margin = "15px";
  childTwo.appendChild(childDiv2);
}
function random() {
  return Math.floor(Math.random() * 12);
}
let Two = document.querySelector(".div2");
Two.addEventListener("click", (e) => {
  if (e.target.className) {
    e.target.style.color = "white";
    e.target.style.textAlign = "center";
    e.target.innerText = e.target.className;
    e.target.style.fontSize = "40px";
    e.target.style.display = "flex";
    e.target.style.justifyContent = "center";
    e.target.style.alignItems = "center";
    setTimeout(() => {
      e.target.innerText = "";
    }, 5000);
  }
});
